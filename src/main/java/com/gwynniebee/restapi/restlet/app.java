package com.gwynniebee.restapi.restlet;

import org.restlet.Application;
import org.restlet.data.MediaType;
import org.restlet.routing.Router;

import com.amazonaws.services.ec2.model.CreateImageRequest;
import com.gwynniebee.rest.service.restlet.GBRestletApplication;
import com.gwynniebee.restapi.restlet.resources.res;

public class app extends GBRestletApplication{

    
    public synchronized Router createInboundRoot() {

        Router router = super.createInboundRoot();
        String resourceUrl = null;
        this.getMetadataService().setDefaultMediaType(MediaType.APPLICATION_JSON);

        resourceUrl = "/getemployeedetails/{uuid}/";
        router.attach(resourceUrl, res.class);
        return router;
    }
}
