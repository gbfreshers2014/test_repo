/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.restapi.restlet.resources;

import org.json.JSONObject;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.MediaType;
import org.restlet.ext.json.JsonpRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.barcode.upload.common.Constants;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;
import com.gwynniebee.restapi.restlet.app;

/**
 * res class. To change this resource rename the class to the
 * required class.
 * @author sarath
 */
public class res extends AbstractServerResource {    
    @Get
    public getemployee test_function() throws Exception
    {
        Request req=getRequest();
        int uuid=Integer.parseInt((String) req.getAttributes().get("uuid"));        
        ResponseStatus restat=new ResponseStatus();
        restat.setCode(200);
        restat.setMessage("Fetch");
        getemployee abc=new getemployee();
        abc.getDetails(uuid);
        abc.setStatus(restat);
        return abc;
    }
    
    @Override
    protected void doInit() {
        super.doInit();
    }  
}
