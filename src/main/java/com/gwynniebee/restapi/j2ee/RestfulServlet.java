/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.restapi.j2ee;

import com.gwynniebee.rest.service.j2ee.GBRestletServlet;
import com.gwynniebee.restapi.restlet.app;

/**
 * @author sarath
 */
@SuppressWarnings("serial")
public class RestfulServlet extends GBRestletServlet<app> {
    /**
     * This is called by tomcat's start of servlet.
     */
    public RestfulServlet() {
        this(new app());
    }

    /**
     * This is called by unit test which create a subclass of this class with subclass of application.
     * @param app application
     */
    public RestfulServlet(app app) {
        super(app);
    }
}
